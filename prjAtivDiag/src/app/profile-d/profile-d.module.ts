import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileDPageRoutingModule } from './profile-d-routing.module';

import { ProfileDPage } from './profile-d.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileDPageRoutingModule
  ],
  declarations: [ProfileDPage]
})
export class ProfileDPageModule {}
