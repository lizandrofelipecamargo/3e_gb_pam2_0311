import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileDPage } from './profile-d.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileDPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileDPageRoutingModule {}
